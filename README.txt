This installation profile is a port of the CMS Joomla! (http://www.joomla.org/).

You can see a demo at GLORilla.com (http://www.glorilla.com/).

The installation profile GLORilla.com is currently only supported for Drupal 5.x, however we will be releasing a version for 6.x as soon as we have the time to make the appropriate changes.

If you have any comments or suggestions, write that to us at Feature Requests (http://www.glorilla.com/forum/10).

This installation profile was developed by http://www.fireflystream.com/


== INSTALLATION ==

In order to utilize this installation profile fully, one should follow the steps below:

Step #1.
* Download and extract drupal-5.x (http://drupal.org)
* Download and extract contributed modules: admin_menu, captcha (with image_captcha, text_captcha sub-modules), fckeditor, image (with image_gallery), autolocale, mimemail, token, pdfview, print, privatemsg, remover, send, pathauto, views (with views_bonus, views_rss and views_ui sub-modules).
Or download and extract glorillacom-5.x-addonly.zip (http://www.glorilla.com/download/glorillacom-5.x-addonly.zip). This archive contain all of the needed additional contributed modules.
* Download and extract the GLORilla.com theme (http://drupal.org/project/glorillacomtheme) onto the 'sites/all/theme/glorillacomtheme' directory
* Copy the GLORilla.com installation profile files (crud.inc, glorillacom.profile) onto the 'profiles/glorillacom' directory
* Download and extract FCKeditor (see FCKeditor-readme.txt) and TCPDF (see tcpdf-readme.txt)

OTHERWISE:
* Download and extract **full GLORilla.com installation package** (http://www.glorilla.com/download/glorillacom-5.7.1.zip).
This archive contain drupal 5.7 files, the GLORilla.com installation profile files and all of the needed additional images and contributed modules.

Step #2.
*Follow the instructions for installing Drupal (see INSTALL.txt in your Drupal site's root) and the first page you get to will offer your choice of installation profiles.  Choose "CMS GLORilla.com Profile for Drupal".

Enjoy!


== NOTE ==
Default login/password of user id=1 (super-admin)

login: admin
password: admin

Change your password immediately to one that only you know after installation.