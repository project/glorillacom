<?php

// vim: filetype=php

/**
 * Installation profile.
 * Date: Mon, 04/01/2008 - 04:01
 *
 */

include_once('crud.inc');

/************************************************************
*                           MODULES                         *
************************************************************/
function glorillacom_profile_modules() {
    return array (
        0 => 'admin_menu',
        1 => 'aggregator',
        2 => 'locale',
        3 => 'block',
        4 => 'blog',
        5 => 'book',
        6 => 'captcha',
        7 => 'comment',
        8 => 'contact',
        9 => 'fckeditor',
        10 => 'filter',
        11 => 'taxonomy',
        12 => 'image',
        13 => 'image_captcha',
        14 => 'image_gallery',
        15 => 'autolocale',
        16 => 'menu',
        17 => 'mimemail',
        18 => 'node',
        19 => 'path',
        20 => 'token',
        21 => 'pdfview',
        22 => 'poll',
        23 => 'print',
        24 => 'privatemsg',
        25 => 'profile',
        26 => 'remover',
        27 => 'search',
        28 => 'send',
        29 => 'system',
        30 => 'forum',
        31 => 'text_captcha',
        32 => 'pathauto',
        33 => 'tracker',
        34 => 'upload',
        35 => 'user',
        36 => 'views',
        37 => 'views_bonus',
        38 => 'views_rss',
        39 => 'views_ui',
        40 => 'watchdog',
      );
}

/************************************************************
*                           DETAILS                         *
************************************************************/
function glorillacom_profile_details() {
    return array (
        'name' => 'CMS GLORilla.com',
        'description' => 'Installation of Ecumenic CMS GLORilla.com',
      );
}

function glorillacom_profile_final() {
/************************************************************
*                          VARIABLES                        *
************************************************************/
    variable_set('anonymous', 'Anonymous');
    variable_set('captcha_administration_mode', 0);
    variable_set('captcha_description_en', 'This question is for testing whether you are a human visitor and to prevent automated spam submissions.');
    variable_set('captcha_log_wrong_responses', 0);
    variable_set('captcha_persistence', '1');
    variable_set('clean_url', '1');
    variable_set('comment_anonymous', '0');
    variable_set('comment_controls', '3');
    variable_set('comment_default_mode', '2');
    variable_set('comment_default_order', '2');
    variable_set('comment_default_per_page', '50');
    variable_set('comment_form_location', '0');
    variable_set('comment_image', '2');
    variable_set('comment_page', 0);
    variable_set('comment_poll', '2');
    variable_set('comment_preview', '1');
    variable_set('comment_story', '2');
    variable_set('comment_subject_field', '1');
    variable_set('comment_test', '2');
    variable_set('file_directory_path', 'files');
    variable_set('file_directory_temp', '/tmp');
    variable_set('file_downloads', '1');
    variable_set('filter_html_1', 1);
    variable_set('forum_block_num_0', '5');
    variable_set('forum_block_num_1', '5');
    variable_set('forum_containers', array (
      0 => 1,
      1 => 2,
      3 => 7,
      4 => 8,
      5 => 1,
    ));
    variable_set('forum_nav_vocabulary', 2);
    variable_set('image_default_path', 'images');
    variable_set('image_gallery_nav_vocabulary', 1);
    variable_set('image_max_upload_size', '800');
    variable_set('image_sizes', array (
      '_original' => 
      array (
        'label' => 'Original',
        'width' => '',
        'height' => '',
        'link' => '1',
      ),
      'thumbnail' => 
      array (
        'label' => 'Thumbnail',
        'width' => '100',
        'height' => '100',
        'link' => '1',
      ),
      'preview' => 
      array (
        'label' => 'Preview',
        'width' => '640',
        'height' => '640',
        'link' => '1',
      ),
    ));
    variable_set('image_updated', 1203885114);
    variable_set('img_assist_create_derivatives', array (
      'properties' => 'properties',
      'custom_advanced' => 'custom_advanced',
      'custom_all' => 'custom_all',
    ));
    variable_set('img_assist_default_insert_mode', 'filtertag');
    variable_set('img_assist_default_label', '100x100');
    variable_set('img_assist_default_link_behavior', 'none');
    variable_set('img_assist_default_link_url', 'http://');
    variable_set('img_assist_link', 'icon');
    variable_set('img_assist_load_description', '1');
    variable_set('img_assist_load_title', '1');
    variable_set('img_assist_max_size', '640x640');
    variable_set('img_assist_page_styling', 'yes');
    variable_set('img_assist_paths', 'node/*
        comment/*');
    variable_set('img_assist_paths_type', '2');
    variable_set('img_assist_popup_label', 'preview');
    variable_set('img_assist_preview_count', '8');
    variable_set('img_assist_textareas_type', '1');
    variable_set('menu_primary_menu', 2);
    variable_set('menu_secondary_menu', 2);
    variable_set('mimemail_engine', 'mimemail');
    variable_set('node_cron_comments_scale', 1);
    variable_set('node_cron_last', '1206606740');
    variable_set('node_cron_last_nid', '8');
    variable_set('node_cron_views_scale', 1);
    variable_set('node_options_forum', array (
      0 => 'status',
    ));
    variable_set('node_options_image', array (
      0 => 'status',
    ));
    variable_set('node_options_page', array (
      0 => 'status',
    ));
    variable_set('node_options_poll', array (
      0 => 'status',
    ));
    variable_set('node_options_story', array (
      0 => 'status',
      1 => 'promote',
    ));
    variable_set('node_options_test', array (
      0 => 'status',
    ));
    variable_set('pathauto_ignore_words', 'a,an,as,at,before,but,by,for,from,is,in,into,like,of,off,on,onto,per,since,than,the,this,that,to,up,via,with');
    variable_set('pathauto_indexaliases', false);
    variable_set('pathauto_indexaliases_bulkupdate', false);
    variable_set('pathauto_max_component_length', '100');
    variable_set('pathauto_max_length', '100');
    variable_set('pathauto_modulelist', array (
      0 => 'node',
      1 => 'user',
      2 => 'taxonomy',
    ));
    variable_set('pathauto_node_bulkupdate', false);
    variable_set('pathauto_node_forum_pattern', '');
    variable_set('pathauto_node_image_pattern', '');
    variable_set('pathauto_node_page_pattern', '');
    variable_set('pathauto_node_pattern', 'content/[title-raw]');
    variable_set('pathauto_node_story_pattern', '');
    variable_set('pathauto_punctuation_hyphen', 1);
    variable_set('pathauto_punctuation_quotes', 0);
    variable_set('pathauto_separator', '-');
    variable_set('pathauto_taxonomy_1_pattern', '');
    variable_set('pathauto_taxonomy_2_pattern', '');
    variable_set('pathauto_taxonomy_applytofeeds', false);
    variable_set('pathauto_taxonomy_bulkupdate', false);
    variable_set('pathauto_taxonomy_pattern', 'category/[vocab-raw]/[catpath-raw]');
    variable_set('pathauto_taxonomy_supportsfeeds', '0/feed');
    variable_set('pathauto_update_action', '2');
    variable_set('pathauto_user_bulkupdate', false);
    variable_set('pathauto_user_pattern', 'users/[user-raw]');
    variable_set('pathauto_user_supportsfeeds', NULL);
    variable_set('pathauto_verbose', false);
    variable_set('pdfview_poll', '1');
    variable_set('print_display_comment', 0);
    variable_set('print_display_poll', 1);
    variable_set('print_display_story', 1);
    variable_set('print_robot_settings', array (
      'noindex' => 1,
      'nofollow' => 1,
      'noarchive' => 0,
      'nocache' => 0,
    ));
    variable_set('print_settings', array (
      'show_link' => '0',
      'show_sys_link' => 1,
      'book_link' => 1,
      'logo_url' => '',
      'css' => '',
      'urls' => 1,
      'comments' => 0,
      'newwindow' => '0',
      'sendtoprinter' => 0,
    ));
    variable_set('print_sourceurl_settings', array (
      'enabled' => 1,
      'date' => 0,
      'forcenode' => 0,
    ));
    variable_set('privatemsg_last_cron', 1206607864);
    variable_set('privatemsg_link_poll', array (
    ));
    variable_set('privatemsg_link_story', array (
    ));
    variable_set('send_poll', 0);
    variable_set('send_poll_linktext', 'send to friend');
    variable_set('send_poll_pernode', 0);
    variable_set('send_story', 1);
    variable_set('send_story_linktext', 'send to friend');
    variable_set('send_story_pernode', 0);
    variable_set('send_test', 0);
    variable_set('send_test_linktext', '<none>');
    variable_set('send_test_pernode', 0);
    variable_set('site_footer', '<p>Copyright &copy; 2008 <a href="http://glorilla.com/">GLORilla.com</a> All Rights Reserved.<br /> <a href="http://glorilla.com/">GLORilla.com</a> is Free Software released under the <a href="http://www.gnu.org/licenses/gpl-2.0.html">GNU/GPL License</a>.</p>');
    variable_set('site_frontpage', 'node');
    variable_set('site_mail', 'admin@glorilla.com');
    variable_set('site_mission', '<p>GLORilla.com 1.5.7 / \'First April Edition\'. It has never been easier to create your own dynamic Web site. Manage all your content from the best CMS admin interface and in virtually any language you speak.</p>');
    variable_set('site_name', 'GLORilla.com - Ecumenic CMS');
    variable_set('site_slogan', 'powered by Joomla and Drupal');
    system_theme_data();
    db_query("UPDATE {system} SET status = 1 WHERE type = 'theme' and name = '%s'", 'glorillacom');
    variable_set('theme_default', 'glorillacom');
    variable_set('theme_glorillacom_settings', array (
      'toggle_logo' => 1,
      'toggle_name' => 1,
      'toggle_slogan' => 1,
      'toggle_mission' => 1,
      'toggle_node_user_picture' => 0,
      'toggle_comment_user_picture' => 0,
      'toggle_search' => 1,
      'toggle_favicon' => 1,
      'default_logo' => 0,
      'logo_path' => 'files/images/logo.gif',
      'logo_upload' => '',
      'default_favicon' => 0,
      'favicon_path' => 'files/images/favicon.ico',
      'favicon_upload' => '',
      'op' => 'Save configuration',
      'form_token' => 'ee95a54425da7619077693308d62e856',
    ));
    variable_set('theme_settings', array (
      'toggle_node_info_page' => false,
    ));
    variable_set('upload_image', '1');
    variable_set('upload_poll', '1');
    variable_set('upload_story', '1');
    variable_set('upload_test', '1');
    variable_set('user_block_max_list_count', '10');
    variable_set('user_block_seconds_online', '900');
    variable_set('user_email_verification', 1);
    variable_set('user_mail_admin_body', '!username,
        
        A site administrator at !site has created an account for you. You may now log in to !login_uri using the following username and password:
        
        username: !username
        password: !password
        
        You may also log in by clicking on this link or copying and pasting it in your browser:
        
        !login_url
        
        This is a one-time login, so it can be used only once.
        
        After logging in, you will be redirected to !edit_uri so you can change your password.
        
        
        --  !site team');
    variable_set('user_mail_admin_subject', 'An administrator created an account for you at !site');
    variable_set('user_mail_approval_body', '!username,
        
        Thank you for registering at !site. Your application for an account is currently pending approval. Once it has been granted, you may log in to !login_uri using the following username and password:
        
        username: !username
        password: !password
        
        You may also log in by clicking on this link or copying and pasting it in your browser:
        
        !login_url
        
        This is a one-time login, so it can be used only once.
        
        After logging in, you may wish to change your password at !edit_uri
        
        
        --  !site team');
    variable_set('user_mail_approval_subject', 'Account details for !username at !site (pending admin approval)');
    variable_set('user_mail_pass_body', '!username,
        
        A request to reset the password for your account has been made at !site.
        
        You may now log in to !uri_brief by clicking on this link or copying and pasting it in your browser:
        
        !login_url
        
        This is a one-time login, so it can be used only once. It expires after one day and nothing will happen if it\'s not used.
        
        After logging in, you will be redirected to !edit_uri so you can change your password.');
    variable_set('user_mail_pass_subject', 'Replacement login information for !username at !site');
    variable_set('user_mail_welcome_body', '!username,
        
        Thank you for registering at !site. You may now log in to !login_uri using the following username and password:
        
        username: !username
        password: !password
        
        You may also log in by clicking on this link or copying and pasting it in your browser:
        
        !login_url
        
        This is a one-time login, so it can be used only once.
        
        After logging in, you will be redirected to !edit_uri so you can change your password.
        
        
        --  !site team');
    variable_set('user_mail_welcome_subject', 'Account details for !username at !site');
    variable_set('user_pictures', '1');
    variable_set('user_picture_default', 'files/pictures/no_photo.gif');
    variable_set('user_picture_dimensions', '85x85');
    variable_set('user_picture_file_size', '30');
    variable_set('user_picture_guidelines', '');
    variable_set('user_picture_path', 'pictures');
    variable_set('user_register', '1');
    variable_set('user_registration_help', '');
    variable_set('views_defaults', array (
      'frontpage' => 'disabled',
      'taxonomy_directory' => 'disabled',
    ));

/************************************************************
*                         NODE TYPES                        *
************************************************************/
    install_add_content_type(array (
      'name' => 'Blog entry',
      'module' => 'blog',
      'description' => 'A blog is a regularly updated journal or diary made up of individual posts shown in reversed chronological order. Each member of the site may create and maintain a blog.',
      'type' => 'blog',
      'has_title' => true,
      'title_label' => 'Title',
      'has_body' => true,
      'body_label' => 'Body',
      'help' => '',
      'min_word_count' => 0,
      'custom' => false,
      'modified' => false,
      'locked' => true,
      'orig_type' => 'blog',
      'is_new' => true,
    ));
    install_add_content_type(array (
      'name' => 'Book page',
      'module' => 'book',
      'description' => 'A book is a collaborative writing effort: users can collaborate writing the pages of the book, positioning the pages in the right order, and reviewing or modifying pages previously written. So when you have some information to share or when you read a page of the book and you didn\'t like it, or if you think a certain page could have been written better, you can do something about it.',
      'type' => 'book',
      'has_title' => true,
      'title_label' => 'Title',
      'has_body' => true,
      'body_label' => 'Body',
      'help' => '',
      'min_word_count' => 0,
      'custom' => false,
      'modified' => false,
      'locked' => true,
      'orig_type' => 'book',
      'is_new' => true,
    ));
    install_add_content_type(array (
      'name' => 'Forum topic',
      'module' => 'forum',
      'description' => 'Create a new topic for discussion in the forums.',
      'title_label' => 'Subject',
      'type' => 'forum',
      'has_title' => true,
      'has_body' => true,
      'body_label' => 'Body',
      'help' => '',
      'min_word_count' => 0,
      'custom' => false,
      'modified' => false,
      'locked' => true,
      'orig_type' => 'forum',
      'is_new' => true,
    ));
    install_add_content_type(array (
      'type' => 'poll',
      'name' => 'Poll',
      'module' => 'poll',
      'description' => 'A poll is a multiple-choice question which visitors can vote on.',
      'help' => '',
      'has_title' => '1',
      'title_label' => 'Question',
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'custom' => '0',
      'modified' => '1',
      'locked' => '1',
      'orig_type' => 'poll',
    ));
    install_add_content_type(array (
      'type' => 'image',
      'name' => 'Image',
      'module' => 'image',
      'description' => 'An image (with thumbnail). This is ideal for publishing photographs or screenshots.',
      'help' => '',
      'has_title' => '1',
      'title_label' => 'Title',
      'has_body' => '1',
      'body_label' => 'Body',
      'min_word_count' => '0',
      'custom' => '0',
      'modified' => '1',
      'locked' => '1',
      'orig_type' => 'image',
    ));
    install_add_content_type(array (
      'type' => 'page',
      'name' => 'Page',
      'module' => 'node',
      'description' => 'If you want to add a static page, like a contact page or an about page, use a page.',
      'help' => '',
      'has_title' => '1',
      'title_label' => 'Title',
      'has_body' => '1',
      'body_label' => 'Body',
      'min_word_count' => '0',
      'custom' => '1',
      'modified' => '1',
      'locked' => '0',
      'orig_type' => 'page',
    ));
    install_add_content_type(array (
      'type' => 'story',
      'name' => 'Story',
      'module' => 'node',
      'description' => 'Stories are articles in their simplest form: they have a title, a teaser and a body, but can be extended by other modules. The teaser is part of the body too. Stories may be used as a personal blog or for news articles.',
      'help' => '',
      'has_title' => '1',
      'title_label' => 'Title',
      'has_body' => '1',
      'body_label' => 'Body',
      'min_word_count' => '0',
      'custom' => '1',
      'modified' => '1',
      'locked' => '0',
      'orig_type' => 'story',
    ));

/************************************************************
*                            ROLES                          *
************************************************************/
    $role_id = array();
    install_set_permissions($role_id['administrator'] = install_add_role('administrator'), array (
      0 => 'access administration menu',
      1 => '  display drupal links',
      2 => '  access news feeds',
      3 => '  administer news feeds',
      4 => '  administer blocks',
      5 => '  use PHP for block visibility',
      6 => '  edit own blog',
      7 => '  create book pages',
      8 => '  create new books',
      9 => '  edit book pages',
      10 => '  edit own book pages',
      11 => '  outline posts in books',
      12 => '  see printer-friendly version',
      13 => '  administer CAPTCHA settings',
      14 => '  skip CAPTCHA',
      15 => '  access comments',
      16 => '  administer comments',
      17 => '  post comments',
      18 => '  post comments without approval',
      19 => '  access site-wide contact form',
      20 => '  access fckeditor',
      21 => '  administer fckeditor',
      22 => '  allow fckeditor file uploads',
      23 => '  administer filters',
      24 => '  administer forums',
      25 => '  create forum topics',
      26 => '  edit own forum topics',
      27 => '  create images',
      28 => '  edit images',
      29 => '  edit own images',
      30 => '  view original images',
      31 => '  administer images',
      32 => '  administer locales',
      33 => '  administer menu',
      34 => '  access content',
      35 => '  administer content types',
      36 => '  administer nodes',
      37 => '  create page content',
      38 => '  create story content',
      39 => '  create test content',
      40 => '  edit own page content',
      41 => '  edit own story content',
      42 => '  edit own test content',
      43 => '  edit page content',
      44 => '  edit story content',
      45 => '  edit test content',
      46 => '  revert revisions',
      47 => '  view revisions',
      48 => '  administer url aliases',
      49 => '  create url aliases',
      50 => '  administer pathauto',
      51 => '  notify of path changes',
      52 => '  access content as PDF',
      53 => '  cancel own vote',
      54 => '  create polls',
      55 => '  inspect all votes',
      56 => '  vote on polls',
      57 => '  access print',
      58 => '  administer print',
      59 => '  access private messages',
      60 => '  administer search',
      61 => '  search content',
      62 => '  use advanced search',
      63 => '  administer send',
      64 => '  send nodes',
      65 => '  view send statistics',
      66 => '  access administration pages',
      67 => '  administer site configuration',
      68 => '  select different theme',
      69 => '  administer taxonomy',
      70 => '  upload files',
      71 => '  view uploaded files',
      72 => '  access user profiles',
      73 => '  administer access control',
      74 => '  administer users',
      75 => '  change own username',
      76 => '  access all views',
      77 => '  administer views',
    ));
    install_set_permissions(1, array (
      0 => 'access news feeds',
      1 => '  see printer-friendly version',
      2 => '  access comments',
      3 => '  post comments',
      4 => '  access site-wide contact form',
      5 => '  access fckeditor',
      6 => '  create forum topics',
      7 => '  access content',
      8 => '  access content as PDF',
      9 => '  vote on polls',
      10 => '  access print',
      11 => '  access private messages',
      12 => '  search content',
      13 => '  send nodes',
      14 => '  view uploaded files',
      15 => '  access user profiles',
      16 => '  access all views',
    ));
    install_set_permissions(2, array (
      0 => 'access news feeds',
      1 => '  edit own blog',
      2 => '  see printer-friendly version',
      3 => '  skip CAPTCHA',
      4 => '  access comments',
      5 => '  post comments',
      6 => '  post comments without approval',
      7 => '  access site-wide contact form',
      8 => '  access fckeditor',
      9 => '  allow fckeditor file uploads',
      10 => '  create forum topics',
      11 => '  edit own forum topics',
      12 => '  create images',
      13 => '  edit own images',
      14 => '  access content',
      15 => '  create story content',
      16 => '  edit own story content',
      17 => '  access content as PDF',
      18 => '  cancel own vote',
      19 => '  vote on polls',
      20 => '  access print',
      21 => '  access private messages',
      22 => '  search content',
      23 => '  use advanced search',
      24 => '  send nodes',
      25 => '  view send statistics',
      26 => '  upload files',
      27 => '  view uploaded files',
      28 => '  access user profiles',
      29 => '  access all views',
    ));
    install_set_permissions($role_id['editor'] = install_add_role('editor'), array (
      0 => 'access news feeds',
      1 => '  administer news feeds',
      2 => '  edit own blog',
      3 => '  create book pages',
      4 => '  create new books',
      5 => '  edit book pages',
      6 => '  edit own book pages',
      7 => '  outline posts in books',
      8 => '  see printer-friendly version',
      9 => '  skip CAPTCHA',
      10 => '  access comments',
      11 => '  administer comments',
      12 => '  post comments',
      13 => '  post comments without approval',
      14 => '  access site-wide contact form',
      15 => '  access fckeditor',
      16 => '  administer fckeditor',
      17 => '  allow fckeditor file uploads',
      18 => '  administer filters',
      19 => '  administer forums',
      20 => '  create forum topics',
      21 => '  edit own forum topics',
      22 => '  create images',
      23 => '  edit images',
      24 => '  edit own images',
      25 => '  view original images',
      26 => '  administer images',
      27 => '  administer menu',
      28 => '  access content',
      29 => '  administer nodes',
      30 => '  create page content',
      31 => '  create story content',
      32 => '  create test content',
      33 => '  edit own page content',
      34 => '  edit own story content',
      35 => '  edit own test content',
      36 => '  edit page content',
      37 => '  edit story content',
      38 => '  edit test content',
      39 => '  administer url aliases',
      40 => '  create url aliases',
      41 => '  access content as PDF',
      42 => '  cancel own vote',
      43 => '  create polls',
      44 => '  inspect all votes',
      45 => '  vote on polls',
      46 => '  access print',
      47 => '  access private messages',
      48 => '  search content',
      49 => '  use advanced search',
      50 => '  send nodes',
      51 => '  view send statistics',
      52 => '  administer taxonomy',
      53 => '  upload files',
      54 => '  view uploaded files',
      55 => '  access user profiles',
      56 => '  administer users',
      57 => '  access all views',
    ));

/************************************************************
*                            USERS                          *
************************************************************/
  db_query("INSERT INTO {users} (uid, name, pass, mail, created, status) VALUES(1, 'admin', '%s', 'admin@mydrupalsite.ru', %d, 1)", md5('admin'), time());
  user_authenticate('admin', 'admin');

/************************************************************
*                            MENUS                          *
************************************************************/

    // Primary links
    install_menu_create_menu_items(array (
      0 => 
      array (
        'path' => 'about_glorillacom',
        'title' => 'About GLORilla.com',
        'description' => 'About GLORilla.com',
        'weight' => '0',
        'type' => '118',
        'children' => 
        array (
        ),
      ),
      1 => 
      array (
        'path' => 'features',
        'title' => 'Features',
        'description' => 'Features',
        'weight' => '0',
        'type' => '118',
        'children' => 
        array (
        ),
      ),
      2 => 
      array (
        'path' => 'news',
        'title' => 'News',
        'description' => 'News',
        'weight' => '0',
        'type' => '118',
        'children' => 
        array (
        ),
      ),
      3 => 
      array (
        'path' => 'the_community',
        'title' => 'The Community',
        'description' => 'The Community',
        'weight' => '0',
        'type' => '118',
        'children' => 
        array (
        ),
      ),
      4 => 
      array (
        'path' => 'forum',
        'title' => 'Forums',
        'description' => '',
        'weight' => '10',
        'type' => '54',
        'children' => 
        array (
        ),
      ),
    ),2);

    // Other menus
    install_menu_create_menu_items(array (
    ),0);

/************************************************************
*                         URL ALIASES                       *
************************************************************/

/*    db_query(
        "INSERT INTO {url_alias} (src,dst)
        VALUES ('%s','%s')",
        'user/1','users/admin'
    );
*/
    db_query(
        "INSERT INTO {url_alias} (src,dst)
        VALUES ('%s','%s')",
        'node/1','content/what-are-requirements-run-glorillacom-15'
    );
    db_query(
        "INSERT INTO {url_alias} (src,dst)
        VALUES ('%s','%s')",
        'node/2','content/glorillacom-community'
    );
    db_query(
        "INSERT INTO {url_alias} (src,dst)
        VALUES ('%s','%s')",
        'node/3','content/whats-new-15'
    );
    db_query(
        "INSERT INTO {url_alias} (src,dst)
        VALUES ('%s','%s')",
        'aggregator/categories/1','news'
    );
    db_query(
        "INSERT INTO {url_alias} (src,dst)
        VALUES ('%s','%s')",
        'node/7','content/glorillacom-used'
    );
    db_query(
        "INSERT INTO {url_alias} (src,dst)
        VALUES ('%s','%s')",
        'node/8','content/glorillacom-first-release'
    );
    db_query(
        "INSERT INTO {url_alias} (src,dst)
        VALUES ('%s','%s')",
        'node/4','content/welcome-frontpage'
    );
    db_query(
        "INSERT INTO {url_alias} (src,dst)
        VALUES ('%s','%s')",
        'node/1','content/glorillacom-used-0'
    );
/************************************************************
*                           BLOCKS                          *
************************************************************/

    install_add_block('user', 0, 'garland', 1, 0, 'left', 0, 0, 0, '', '');
    install_add_block('user', 1, 'garland', 1, 0, 'left', 0, 0, 0, '', '');
    install_add_block('user', 0, 'garland', 1, 0, 'left', 0, 0, 0, '0', 'Login form');
    install_add_block('user', 1, 'garland', 1, 0, 'left', 0, 0, 0, '0', '');
    install_add_block('img_assist', 0, 'droomla.com', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('image', 1, 'droomla.com', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('image', 0, 'droomla.com', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('user', 3, 'droomla.com', 1, 0, 'left', 1, 0, 0, '0', '');
    install_add_block('user', 2, 'droomla.com', 1, 0, 'header', 0, 0, 0, '0', '');
    install_add_block('user', 1, 'droomla.com', 1, 0, 'left', 0, 0, 0, '0', '');
    install_add_block('search', 0, 'droomla.com', 1, 0, 'right', 0, 0, 0, '0', '');
    install_add_block('user', 0, 'droomla.com', 1, 10, 'left', 0, 0, 0, '0', 'Login form');
    install_add_block('profile', 0, 'droomla.com', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('poll', 0, 'droomla.com', 1, 0, 'right', 1, 0, 0, '0', '');
    install_add_block('node', 0, 'droomla.com', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('menu', 2, 'droomla.com', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('forum', 1, 'droomla.com', 0, 0, '', 1, 0, 0, '0', '');
    install_add_block('forum', 0, 'droomla.com', 0, 0, '', 1, 0, 0, '0', '');
    install_add_block('comment', 0, 'droomla.com', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('book', 0, 'droomla.com', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('blog', 0, 'droomla.com', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('aggregator', -1, 'droomla.com', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('aggregator', -1, 'droomla.com', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('image', 0, 'droomlacom', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('user', 3, 'droomlacom', 1, 0, 'header2', 1, 0, 0, '0', '');
    install_add_block('user', 2, 'droomlacom', 1, 0, 'header', 0, 0, 0, '0', '');
    install_add_block('user', 1, 'droomlacom', 1, 0, 'left', 0, 0, 0, '0', '');
    install_add_block('user', 0, 'droomlacom', 1, 10, 'left', 0, 0, 0, '0', 'Login form');
    install_add_block('search', 0, 'droomlacom', 1, 0, 'right', 0, 0, 0, '0', '');
    install_add_block('profile', 0, 'droomlacom', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('poll', 0, 'droomlacom', 1, 0, 'right', 1, 0, 0, '0', '');
    install_add_block('menu', 2, 'droomlacom', 1, 0, 'left', 0, 0, 0, '0', '');
    install_add_block('node', 0, 'droomlacom', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('forum', 1, 'droomlacom', 0, 0, '', 1, 0, 0, '0', '');
    install_add_block('forum', 0, 'droomlacom', 0, 0, '', 1, 0, 0, '0', '');
    install_add_block('comment', 0, 'droomlacom', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('book', 0, 'droomlacom', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('blog', 0, 'droomlacom', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('aggregator', -1, 'droomlacom', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('aggregator', -1, 'droomlacom', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('image', 1, 'droomlacom', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('views', send_recently_emailed, 'glorillacom', 0, 0, '', 0, 0, 0, '', '');
    install_add_block('views', send_most_emailed, 'glorillacom', 0, 0, '', 0, 0, 0, '', '');
    install_add_block('views', comments_recent, 'glorillacom', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('privatemsg', 0, 'glorillacom', 1, -10, 'right', 1, 0, 0, '0', '');
    install_add_block('image', 1, 'glorillacom', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('image', 0, 'glorillacom', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('user', 3, 'glorillacom', 1, 0, 'right', 1, 0, 0, '0', '');
    install_add_block('user', 1, 'glorillacom', 1, -10, 'left', 0, 0, 0, '0', '');
    install_add_block('user', 2, 'glorillacom', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('user', 0, 'glorillacom', 1, -10, 'right', 0, 0, 0, '0', 'Login form');
    install_add_block('search', 0, 'glorillacom', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('profile', 0, 'glorillacom', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('poll', 0, 'glorillacom', 1, -1, 'right', 1, 0, 0, '0', '');
    install_add_block('node', 0, 'glorillacom', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('menu', 2, 'glorillacom', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('forum', 1, 'glorillacom', 1, 0, 'header2', 1, 0, 0, '0', '');
    install_add_block('forum', 0, 'glorillacom', 1, 0, 'header', 1, 0, 0, '0', '');
    install_add_block('comment', 0, 'glorillacom', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('book', 0, 'glorillacom', 0, 0, '', 0, 0, 0, '0', '');
    install_add_block('blog', 0, 'glorillacom', 0, 0, '', 0, 0, 0, '0', '');

/************************************************************
*                       EXPORTING NODES                     *
************************************************************/
    // exporting nodes of type: Page
    db_query(
        "INSERT INTO {node} (nid,vid,type,title,uid,status,created,changed,comment,promote,moderate,sticky)
        VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
        '1','1','page','What are the requirements to run GLORilla.com 1.5.7?','1','1','1207037041','1206963989','0','0','0','0'
    );
    db_query(
        "INSERT INTO {node} (nid,vid,type,title,uid,status,created,changed,comment,promote,moderate,sticky)
        VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
        '2','2','page','The GLORilla.com Community','1','1','1207037041','1206964059','0','0','0','0'
    );
    db_query(
        "INSERT INTO {node} (nid,vid,type,title,uid,status,created,changed,comment,promote,moderate,sticky)
        VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
        '3','3','page',' What\'s New In 1.5.7? ','1','1','1207037041','1206964118','0','0','0','0'
    );
    db_query(
        "INSERT INTO {node_revisions} (nid,vid,uid,title,body,teaser,log,timestamp,format)
        VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s')",
        '1','1','1','What are the requirements to run GLORilla.com 1.5.7?','<p>GLORilla.com runs on the PHP pre-processor. PHP comes in many flavours, for a lot of operating systems. Beside PHP you will need a Web server. GLORilla.com is optimized for the Apache Web server, but it can run on different Web servers like Microsoft IIS it just requires additional configuration of PHP and MySQL. GLORilla.com also depends on a database, for this currently you can use MySQL and Postgress.     Many people know from their own experience that it\'s not easy to install an Apache Web server and it gets harder if you want to add MySQL, PHP and Perl. XAMPP, WAMP, and MAMP are easy to install distributions containing Apache, MySQL, PHP and Perl for the Windows, Mac OSX and Linux operating systems. These packages are for localhost installations on non-public servers only.     The minimum version requirements are:              * Apache 1.x or 2.x         * PHP 4.3 or up         * MySQL 3.23 or up          For the latest minimum requirements details visit the <a href="http://glorilla.com/">GLORilla.com Help Site</a> and <a href="http://glorilla.com/forum">Forums</a></p><p>&nbsp;</p>','<p>GLORilla.com runs on the PHP pre-processor. PHP comes in many flavours, for a lot of operating systems. Beside PHP you will need a Web server. GLORilla.com is optimized for the Apache Web server, but it can run on different Web servers like Microsoft IIS it just requires additional configuration of PHP and MySQL. GLORilla.com also depends on a database, for this currently you can use MySQL and Postgress.     Many people know from their own experience that it\'s not easy to install an Apache Web server and it gets harder if you want to add MySQL, PHP and Perl.','','1206963989','3'
    );
    db_query(
        "INSERT INTO {node_revisions} (nid,vid,uid,title,body,teaser,log,timestamp,format)
        VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s')",
        '2','2','1','The GLORilla.com Community','<p><strong>Got a question?</strong> With more than 140,000 members, the online forums at <a href="http://glorilla.com/forum">GLORilla.com/forum</a> are a great resource for both new and experienced users. Go ahead, ask your toughest questions, the community is waiting to see what you\'re going to do with your GLORilla.com site.          <strong>Do you want to show off your new GLORilla.com Web site?</strong> Go ahead, we have a section dedicated to that on our forum.          <strong>Do you want to join in?</strong>          If you think working with GLORilla.com is fun, wait until you start working on it. We\'re passionate about helping GLORilla.com. Users make the jump to becoming contributing members of the community, so there are many ways you can help GLORilla.com\'s development:</p><ul>         <li>Submit news about GLORilla.com. We syndicate all GLORilla.com related news on our news portal. If you have some GLORilla.com news that you would like to share with the community, please submit your short story, article, announcement or review here.</li><li>Report bugs and request features in our trackers. Please read Reporting Bugs, for details on how we like our bug reports served up</li><li>Submit patches for new and/or fixed behaviour. Please read Submitting Patches, for details on how to submit a patch.</li><li>Join the developer forums and share your ideas for how to improve GLORilla.com. We\'re always open to suggestions, although we\'re likely to be sceptical of large-scale suggestions without some code to back it up.</li><li>Join any of the GLORilla.com Working Groups and bring your personal expertise to the GLORilla.com community. More info about the different working groups can be found on the Working Group Roll Call.</li></ul><p>That\'s all you need to know if you\'d like to join the GLORilla.com development community.</p>','<p><strong>Got a question?</strong> With more than 140,000 members, the online forums at <a href="http://glorilla.com/forum">GLORilla.com/forum</a> are a great resource for both new and experienced users. Go ahead, ask your toughest questions, the community is waiting to see what you\'re going to do with your GLORilla.com site.          <strong>Do you want to show off your new GLORilla.com Web site?</strong> Go ahead, we have a section dedicated to that on our forum.','','1206964059','3'
    );
    db_query(
        "INSERT INTO {node_revisions} (nid,vid,uid,title,body,teaser,log,timestamp,format)
        VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s')",
        '3','3','1',' What\'s New In 1.5.7? ','<p>As with previous releases, GLORilla.com provides a unified and easy-to-use framework for delivering content for Web sites of all kinds. To support the changing nature of the Internet and emerging Web technologies, GLORilla.com required substantial restructuring of its core functionality and we also used this effort to simplify many challenges within the current user interface. GLORilla.com 1.5.7 has many new features.          The goals for the GLORilla.com 1.5.7 release are to:</p><ul>         <li>Substantially improve usability, manageability, and scalability.</li><li>Expand accessibility to support internationalisation, double-byte characters and Right-to-Left support for Arabic, Farsi, and Hebrew languages for example amongst others.</li><li>Extend the integration of external applications through Web Services and remote authentication such as the Lightweight Directory Access Protocol (LDAP).</li><li>Enhance the content delivery, template and presentation capabilities to support accessibility standards and content delivery to any destination.</li><li>Achieve a more sustainable and flexible framework for Component and Extension developers.</li><li>Deliver backward compatibility with previous releases of Components, Templates, Modules and other Extensions.</li></ul><p>&nbsp;</p>','<p>As with previous releases, GLORilla.com provides a unified and easy-to-use framework for delivering content for Web sites of all kinds. To support the changing nature of the Internet and emerging Web technologies, GLORilla.com required substantial restructuring of its core functionality and we also used this effort to simplify many challenges within the current user interface. GLORilla.com 1.5.7 has many new features.          The goals for the GLORilla.com 1.5.7 release are to:</p>','','1206964118','3'
    );
    // exporting nodes of type: Story
    db_query(
        "INSERT INTO {node} (nid,vid,type,title,uid,status,created,changed,comment,promote,moderate,sticky)
        VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
        '4','4','story','Welcome to the Frontpage','1','1','1207037041','1206963917','2','1','0','1'
    );
    db_query(
        "INSERT INTO {node_revisions} (nid,vid,uid,title,body,teaser,log,timestamp,format)
        VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s')",
        '4','4','1','Welcome to the Frontpage','<p>GLORilla.com is a free open source framework and content publishing system designed for quickly creating highly interactive multi-language Web sites, online communities, media portals, blogs and eCommerce applications.  GLORilla.com provides an easy-to-use graphical user interface that simplifies the management and publishing of large volumes of content including HTML, documents, and rich media. GLORilla.com is used by organisations of all sizes for Public Web sites, Intranets, and Extranets and is supported by a community of thousands of users.</p>','<p>GLORilla.com is a free open source framework and content publishing system designed for quickly creating highly interactive multi-language Web sites, online communities, media portals, blogs and eCommerce applications.  GLORilla.com provides an easy-to-use graphical user interface that simplifies the management and publishing of large volumes of content including HTML, documents, and rich media. GLORilla.com is used by organisations of all sizes for Public Web sites, Intranets, and Extranets and is supported by a community of thousands of users.</p>','','1206963917','1'
    );

    system_initialize_theme_blocks('glorillacom');

    return;
}

?>
