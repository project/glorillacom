TCPDF - README
============================================================

Name:
	TCPDF

Version:
	2.9.000
	
Release date:
	2008-03-26

Author:
	Nicola Asuni 
	
Copyright (c) 2001-2008:
	Nicola Asuni
	Tecnick.com s.r.l.
	Via Della Pace, 11
	09044 Quartucciu (CA)
	ITALY
	www.tecnick.com
	
URLs:
	http://www.tcpdf.org
	http://tcpdf.sourceforge.net/
	
Description:
	TCPDF is a PHP class for generating PDF files on-the-fly without requiring external extensions.
	TCPDF has been originally derived from the Public Domain FPDF class by Olivier Plathey (http://www.fpdf.org).
	
Main Features:
	* no external libraries are required for the basic functions;
	* supports all ISO page formats;
	* supports UTF-8 Unicode and Right-To-Left languages;
	* supports document encryption;
	* includes methods to publish some xhtml code;
	* includes graphic and transformation methods;
	* includes bookmarks;
	* includes Javascript and forms support;
	* includes a method to print various barcode formats (requires GD library);
	* supports TrueTypeUnicode, TrueType, Type1 and encoding;
	* supports custom page formats, margins and units of measure;
	* includes methods for page header and footer management;
	* supports automatic page break;
	* supports automatic page numbering;
	* supports automatic line break and text justification;
	* supports JPEG, PNG anf GIF images;
	* supports colors;
	* supports links;
	* supports page compression (requires zlib extension);
	* supports PDF user's rights.

Installation (full instructions on http://www.tcpdf.org):
	1. copy the folder on your Web server
	2. set your installation path on the config/tcpdf_config.php
	3. call the example_001.php page with your browser to see an example

Source Code Documentation:
	doc/index.html
	
For Additional Documentation check:
	http://www.tcpdf.org

License
	GNU LESSER GENERAL PUBLIC LICENSE v.2.1
	http://www.gnu.org/copyleft/lesser.html
============================================================